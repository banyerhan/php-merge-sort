<?php
	
	/**
	 * Initialise a data array with a sequential set of integers, then shuffle to create an un-sorted 
	 * list for the purpose of this example..
	 */
	for ($i = 1; $i <= 20; $i++) {
		$data[] = $i;
	}

	shuffle($data);

	// Output the initially un-sorted list.
	echo "<pre>";
	print_r($data);
	echo "</pre>";	

	// Determine the start time of the function.
	$start = microtime(true);

	// Sort the data.
	$sortedData = mergeSort($data);

	// Calculate the time taken to execute the sort function
	$duration = microtime(true) - $start;

	echo "Duration : $duration s";

	// Output the sorted data.
	echo "<pre>";
	print_r($sortedData);
	echo "</pre>";

	/**
	 * Perform a merge sort on an array of simple data types.
	 *
	 * @param array $data This is an array of simple data types, e.g. integers
	 * @return array $sortedData
	 */
	function mergeSort($data) {
		$sortedData = array(); 

		/**
		 * Only sort data from an array that has more than 1 element. This is relevant as the 
		 * function is called recursively, splitting the data set on each execution.
		 */
		if (count($data) > 1) {
			// Determine the middle point of the array for separation.
			$dataMidPoint = floor(count($data)/2);

			// Process the first half of the data array.
			$dataSetA = mergeSort(array_slice($data, 0, $dataMidPoint));

			// Process the second half of the datat array.
			$dataSetB = mergeSort(array_slice($data, $dataMidPoint, count($data)));

			$counterA = $counterB = 0;

			for ($i = 0; $i < count($data); $i++) {
				if ($counterA >= count($dataSetA)) {
					/**
					 * All the elements from the first half of the data array have already been 
					 * sorted, start adding the elements from the second half of the data array to 
					 * the sorted list.
					 */
					$sortedData[] = $dataSetB[$counterB];
					$counterB++;
				} else if ($counterB >= count($dataSetB) || $dataSetA[$counterA] <= $dataSetB[$counterB]) {
					/**
					 * All the elements from the second half of the data array have been processed 
					 * OR the next element from the first half should be placed in the sorted list 
					 * as it is next in the overall sequence. 
					 */
					$sortedData[] = $dataSetA[$counterA];
					$counterA++;
				} else {
					$sortedData[] = $dataSetB[$counterB];
					$counterB++;
				}
			}
		} else {
			$sortedData = $data;
		}

		return $sortedData;
	}

?>